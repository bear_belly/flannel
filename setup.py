#!/usr/bin/env python3
"""
This file is part of flannel.

flannel is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

flannel is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with flannel.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import re
from setuptools import setup, find_packages

HERE = os.path.abspath(os.path.dirname(__file__))
REQ = os.path.abspath(os.path.join(HERE, 'requirements.txt'))
README = os.path.abspath(os.path.join(HERE, 'README.rst'))


def read_requirements(path):
    if not os.path.exists(path):
        return []
    with open(path) as requirements:
        return [x.strip() for x in requirements.read().split('\n')
                if re.match(r'^[\d\w]', x)]

def get_readme(path):
    if not os.path.exists(path):
        return []
    with open(path) as readme:
        return readme.read()


setup(
    name="flannel",
    version="1.2",
    packages=find_packages(),
    install_requires=read_requirements(REQ),

    # metadata for upload to PyPI
    author="Jordan Hewitt",
    author_email="jordan.h@startmail.com",
    description=get_readme(README),
    license="GPLv3",
    keywords="flannel qt lot viewer gui application app logging",
    entry_points={
        "gui_scripts": [
            'flannel = flannel.main:main'
        ]
    }
)
